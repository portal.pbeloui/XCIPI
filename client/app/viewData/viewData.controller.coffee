angular.module 'excipiensApp'
  .controller 'viewSourceCtrl', ($scope, $uibModalInstance, Auth, source) ->
    $scope.source = source

    $scope.cancel = ->
      $uibModalInstance.dismiss 'cancel'