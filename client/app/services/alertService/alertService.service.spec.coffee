'use strict'

describe 'Service: alertService', ->

  # load the service's module
  beforeEach module 'excipiensApp'

  # instantiate service
  alertService = undefined
  beforeEach inject (_alertService_) ->
    alertService = _alertService_

  it 'should do something', ->
    expect(!!alertService).toBe true