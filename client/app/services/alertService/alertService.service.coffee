'use strict'

angular.module 'excipiensApp'
.service 'alertService', ($rootScope,$timeout) ->

  alerts = []
  
  add: (type, msg) ->
    alerts = []
    alerts.push
      type: type
      msg: msg
      close: ->
        closeAlert this
    $timeout(@closeAlert, 4500, true, alert);
    $rootScope.$emit("updateAlerts")

  closeAlert: (alert) ->
    alerts.splice alerts.indexOf(alert), 1
    $rootScope.$emit("updateAlerts")

  clear: ->
    alerts = []

  get: ->
    alerts