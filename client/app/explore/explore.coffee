'use strict'

angular.module 'excipiensApp'
.config ($stateProvider) ->
  $stateProvider.state 'explore',
    url: '/explore'
    templateUrl: 'app/explore/explore.html'
    controller: 'ExploreCtrl'
