'use strict'

angular.module 'excipiensApp'
.controller 'ExploreCtrl', ($scope,$http, socket) ->

  $scope.sources = []
  $http.get('/api/sources').success (sources) ->
    $scope.sources = sources
    socket.syncUpdates 'source', $scope.sources
