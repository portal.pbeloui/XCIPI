'use strict'

describe 'Controller: ExploreCtrl', ->

  # load the controller's module
  beforeEach module 'excipiensApp'
  ExploreCtrl = undefined
  scope = undefined

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ExploreCtrl = $controller 'ExploreCtrl',
      $scope: scope

  it 'should ...', ->
    expect(1).toEqual 1
