'use strict'

angular.module 'excipiensApp'
.controller 'VisualiseSourceCtrl', ($scope, $http, $filter) ->

  $scope.deleteSource = () ->
    $http.delete '/api/sources/' + $scope.source._id

  $scope.revert = () ->
    $http.get '/api/sources/' + $scope.source._id + '/revert/'

  $scope.droppedXObject = null
  $scope.droppedYObject = null

  $scope.removeX = ->
    $scope.droppedXObject = null
    $scope.axisChanged()
  $scope.removeY = ->
    $scope.droppedYObject = null
    $scope.axisChanged()

  $scope.onXDropComplete = (field, evt) ->
    if field.is_identifier
      $scope.droppedXObject = field
      $scope.axisChanged()
  $scope.onYDropComplete = (field, evt) ->
    if field.type == "Number"
      $scope.droppedYObject = field
      $scope.axisChanged()

  $scope.changeOrder = (axis, direction) ->
    switch axis
      when 'x'
        switch direction
          when 'asc'
            $scope.data = $filter('orderBy')($scope.data, "#{$scope.droppedXObject.inner_id}", true)
          when 'desc'
            $scope.data = $filter('orderBy')($scope.data, "#{$scope.droppedXObject.inner_id}", false)
      when 'y'
        switch direction
          when 'asc'
            $scope.data = $filter('orderBy')($scope.data, "#{$scope.droppedYObject.inner_id}", true)
          when 'desc'
            $scope.data = $filter('orderBy')($scope.data, "#{$scope.droppedYObject.inner_id}", false)
    $scope.redraw()

  $scope.chartObject = {}
  $scope.chartObject.type = 'ColumnChart'

  $scope.chartObject.data =
    'cols': [
      {
        id: 't'
        label: 'Topping'
        type: 'string'
      }
      {
        id: 's'
        label: 'Score'
        type: 'number'
      }
    ]
    'rows': []

  $scope.chartObject.options = 
    is3D: true
    title: ''
    legend: 'none'
    animation:
      duration: 150
    backgroundColor: { fill:'transparent' }

  $scope.axisChanged = ->  
    if $scope.droppedXObject != null and $scope.droppedYObject != null

      $http.get("/api/sources/#{$scope.source._id}/data")
        .success (data) ->
          $scope.data = data
          $scope.chartObject.data =
            'cols': [
              {
                id: 't'
                label: $scope.droppedXObject.name
                type:  $scope.droppedXObject.type.toLowerCase()
              }
              {
                id: 's'
                label: $scope.droppedYObject.name
                type: $scope.droppedYObject.type.toLowerCase()
              }
            ]
            'rows': []
          $scope.redraw()

  $scope.redraw = ->
    $scope.chartObject.data.rows = []
    for object in $scope.data
      do (object) ->
        sentimentValue = parseFloat(object['score'], 10).toFixed(2)
        $scope.chartObject.data['rows'].push {c: [{v: object[$scope.droppedXObject.inner_id]},{v: object[$scope.droppedYObject.inner_id]}]}
