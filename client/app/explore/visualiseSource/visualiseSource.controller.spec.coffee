'use strict'

describe 'Controller: VisualiseSourceCtrl', ->

  # load the controller's module
  beforeEach module 'excipiensApp'
  VisualiseSourceCtrl = undefined
  scope = undefined

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    VisualiseSourceCtrl = $controller 'VisualiseSourceCtrl',
      $scope: scope

  it 'should ...', ->
    expect(1).toEqual 1
