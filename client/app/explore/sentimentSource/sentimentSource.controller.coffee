'use strict'

angular.module 'excipiensApp'
.controller 'SentimentSourceCtrl', ($scope, $http, $filter) ->

  $scope.revert = () ->
    $http.get '/api/sources/' + $scope.source._id + '/revert/'

  $scope.remove = ->
    $scope.droppedObject = null
    $scope.axisChanged()

  $scope.onDropComplete = (field, evt) ->
      $scope.droppedObject = field
      $scope.axisChanged()

  $scope.changeOrder = (axis, direction) ->
    
    switch axis
      when 'x'
        switch direction
          when 'asc'
            $scope.data = $filter('orderBy')($scope.data, "#{$scope.droppedXObject.inner_id}", true)
          when 'desc'
            $scope.data = $filter('orderBy')($scope.data, "#{$scope.droppedXObject.inner_id}", false)
      when 'y'
        switch direction
          when 'asc'
            $scope.data = $filter('orderBy')($scope.data, "score", true)
          when 'desc'
            $scope.data = $filter('orderBy')($scope.data, "score", false)
    $scope.redraw()

  $scope.droppedXObject = null
  $scope.droppedYObject = null

  $scope.removeX = ->
    $scope.droppedXObject = null
    $scope.axisChanged()
  $scope.removeY = ->
    $scope.droppedYObject = null
    $scope.axisChanged()

  $scope.onXDropComplete = (field, evt) ->
    if field.is_identifier
      $scope.droppedXObject = field
      $scope.axisChanged()
  $scope.onYDropComplete = (field, evt) ->
    if field.type == "String"
      $scope.droppedYObject = field
      $scope.axisChanged()

  $scope.chartObject = {}
  $scope.chartObject.type = 'ColumnChart'

  $scope.axisChanged = ->
    if $scope.droppedXObject != null and $scope.droppedYObject != null
      $http.get("/api/sources/#{$scope.source._id}/analyse_field_sentiment/#{$scope.droppedYObject._id}/#{$scope.droppedXObject._id}")
        .success (data) ->
          convertedData = []
          for object in data
            do (object) ->
              object['score'] = parseFloat(object['score'], 10).toFixed(2)
              if !isFinite(object['score'])
                object['score'] = 0
              convertedData.push object

          $scope.data = convertedData
          if $scope.data
            $scope.chartObject.type = 'ColumnChart'

            $scope.chartObject.options = 
              title: ''
              vAxis: {format:'0.00', maxValue: 1, minValue: -1}
              legend: 'none'
              animation:
                duration: 150
              backgroundColor: { fill:'transparent' }

            $scope.chartObject.data =
              cols: [
                {
                  id: 't'
                  label: $scope.droppedXObject.name
                  type: 'string'
                }
                {
                  id: 's'
                  label: "Sentiment for #{$scope.droppedYObject.name}"
                  type: 'number'
                }
                {role: "style", type: "string"}
              ]
              rows: []

            $scope.redraw()
  
  $scope.redraw = ->
    $scope.chartObject.data.rows = []
    console.log $scope.data
    for object in $scope.data
      do (object) ->
        colour = switch
          when object['score'] <= -0.5 then 'red'
          when (object['score'] < 0 and object['score'] > -0.5) then 'orange'                  
          when object['score'] >= 0 then 'green'
        $scope.chartObject.data['rows'].push {c: [{v: "#{object[$scope.droppedXObject.inner_id].substring(0, 25)}"},{v: object['score']},{v: colour}]}

