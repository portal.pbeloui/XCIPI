'use strict'

angular.module 'excipiensApp'
.controller 'OrganizeCtrl', ($scope, $http, socket) ->

  $scope.deleteSource = (source) ->
    $http.delete '/api/sources/' + source._id

  $http.get('/api/sources').success (sources) ->
    $scope.sources = sources
    socket.syncUpdates 'source', $scope.sources

  $scope.deleteSource = (source) ->
    $http.delete '/api/sources/' + source._id

  $scope.$on '$destroy', ->
    socket.unsyncUpdates 'source'