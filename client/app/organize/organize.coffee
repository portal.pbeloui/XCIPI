'use strict'

angular.module 'excipiensApp'
.config ($stateProvider) ->
  $stateProvider.state 'organize',
    url: '/organize'
    templateUrl: 'app/organize/organize.html'
    controller: 'OrganizeCtrl'
