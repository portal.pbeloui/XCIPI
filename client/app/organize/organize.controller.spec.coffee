'use strict'

describe 'Controller: OrganizeCtrl', ->

  # load the controller's module
  beforeEach module 'excipiensApp'
  OrganizeCtrl = undefined
  scope = undefined

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    OrganizeCtrl = $controller 'OrganizeCtrl',
      $scope: scope

  it 'should ...', ->
    expect(1).toEqual 1
