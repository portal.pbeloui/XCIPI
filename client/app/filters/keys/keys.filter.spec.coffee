'use strict'

describe 'Filter: keys', ->

  # load the filter's module
  beforeEach module 'excipiensApp'

  # initialize a new instance of the filter before each test
  keys = undefined
  beforeEach inject ($filter) ->
    keys = $filter 'keys'

  it 'should return the input prefixed with \'keys filter:\'', ->
    text = 'angularjs'
    expect(keys text).toBe 'keys filter: ' + text
