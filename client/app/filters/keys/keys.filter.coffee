'use strict'

angular.module 'excipiensApp'
.filter 'keys', ->
  (input) ->
    if !angular.isObject(input)
      throw Error('Usage of non-objects with keylength filter.')
    keys = Object.keys(input)
    keys.splice(keys.indexOf("$$hashKey", 1))
    keys