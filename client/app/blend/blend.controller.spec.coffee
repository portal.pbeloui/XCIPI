'use strict'

describe 'Controller: BlendCtrl', ->

  # load the controller's module
  beforeEach module 'excipiensApp'
  BlendCtrl = undefined
  scope = undefined

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    BlendCtrl = $controller 'BlendCtrl',
      $scope: scope

  it 'should ...', ->
    expect(1).toEqual 1
