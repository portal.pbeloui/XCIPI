'use strict'

angular.module 'excipiensApp'
.controller 'BlendCtrl', ($scope, $http, socket, alertService) ->

  $scope.sources = []

  $http.get('/api/sources').success (sources) ->
    $scope.sources = sources 
    socket.syncUpdates 'source', $scope.sources

  $scope.$on '$destroy', ->
   socket.unsyncUpdates 'source'

  $scope.joinFields = []

  $scope.filterField = null

  $scope.onDropBlendComplete = (data, evt) ->

      if data.is_identifier
        alreadyHasField  = false
        alreadyHasSource = false
      
        for key, value of $scope.joinFields

          if value.source._id == data.source._id
            alreadyHasSource = true 
          if value._id == data._id
            alreadyHasField = true
        
        if not alreadyHasField

          if not alreadyHasSource
            $scope.joinFields.push data
          else
            alertService.add('warning', 'You already have a field from this source in this blend')
        else
          alertService.add('warning', 'You cannot use the same field twice to a blend')
      else
        alertService.add('warning', 'You can only use idenfifying fields in a blend')

  $scope.blend = () ->
    if $scope.joinFields.length > 1
      $http.post(
        '/api/sources/blend/'
        $scope.joinFields
      ).then ((response) ->
        console.log "updated"
        console.log response.data
        alertService.add('success', "the blend was successful and the source '#{response.data.name}' was created")
      ), (response) ->
        # called asynchronously if an error occurs
        # or server returns response with an error status. 

  $scope.removeField = (field) ->
    for key, value of $scope.joinFields
      if value._id == field._id
        $scope.joinFields.splice(key, 1)

