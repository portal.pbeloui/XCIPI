'use strict'

angular.module 'excipiensApp'
.config ($stateProvider) ->
  $stateProvider.state 'blend',
    url: '/blend'
    templateUrl: 'app/blend/blend.html'
    controller: 'BlendCtrl'
