'use strict'

angular.module 'excipiensApp'
.config ($stateProvider) ->
  $stateProvider.state 'filter',
    url: '/filter'
    templateUrl: 'app/filter/filter.html'
    controller: 'FilterCtrl'
