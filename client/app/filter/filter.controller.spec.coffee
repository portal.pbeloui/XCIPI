'use strict'

describe 'Controller: FilterCtrl', ->

  # load the controller's module
  beforeEach module 'excipiensApp'
  FilterCtrl = undefined
  scope = undefined

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    FilterCtrl = $controller 'FilterCtrl',
      $scope: scope

  it 'should ...', ->
    expect(1).toEqual 1
