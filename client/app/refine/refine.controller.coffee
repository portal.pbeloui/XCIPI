'use strict'

angular.module 'excipiensApp'
.controller 'RefineCtrl', ($scope, $http, socket) ->

  $http.get('/api/sources').success (sources) ->
    $scope.sources = sources
    socket.syncUpdates 'source', $scope.sources

  $scope.$on '$destroy', ->
    socket.unsyncUpdates 'source'