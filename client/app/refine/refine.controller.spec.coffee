'use strict'

describe 'Controller: RefineCtrl', ->

  # load the controller's module
  beforeEach module 'excipiensApp'
  RefineCtrl = undefined
  scope = undefined

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    RefineCtrl = $controller 'RefineCtrl',
      $scope: scope

  it 'should ...', ->
    expect(1).toEqual 1
