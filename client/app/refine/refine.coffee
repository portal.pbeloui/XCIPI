'use strict'

angular.module 'excipiensApp'
.config ($stateProvider) ->
  $stateProvider.state 'refine',
    url: '/refine'
    templateUrl: 'app/refine/refine.html'
    controller: 'RefineCtrl'
