'use strict'

angular.module 'excipiensApp'
.controller 'UploaderCtrl', ($scope, Upload, $timeout, $http, socket, Auth, alertService) ->

  file = $scope.file
  $scope.columns = []  
  $scope.parseComplete = false
  $scope.aborted = false

  $scope.source = null
  $scope.upload = null

  $scope.name = $scope.file.name

  $scope.abortUpload = ->
    $scope.upload.abort()
    $scope.aborted = true

  parse = ->
    #create source
    $http.post(
      '/api/sources', 
      name:    $scope.name
      user_id: Auth.getCurrentUser()._id
      type:    file.type
      key:     $scope.key
    ).then ((response) ->
        $scope.source = response.data
        $http(
          url: "api/sources/#{response.data._id}/parse/"
          method: 'GET'
        ).then ((response) ->
          $scope.fields = response.data
          $scope.parseComplete = true
          alertService.add('success', "the upload source #{$scope.source.name} was successfully created and parsed")
        ), (response) ->
          #error
          alertService.add('error', "a error occured while trying to process the file #{$scope.name}: #{response}")
    ), (response) ->
      # called asynchronously if an error occurs
      # or server returns response with an error status.  	

  $scope.key = ""

  $http(
    url: "api/sources/sign",
    method: 'GET'
    ).then ((response) ->
      data = response.data
      $scope.upload = upload(data)
  ), (response) ->
    #error
    console.log 'error'

  upload = (data) ->
    $scope.key = data['key']
    upload = Upload.upload(
      url: 'https://excipiens-dev.s3.amazonaws.com/'
      method: 'POST'
      fields:
        key:              data['key']
        AWSAccessKeyId:   data['AWSAccessKeyId']
        acl:              'public-read'
        policy:           data['policy']
        signature:        data['signature']
        'Content-Length': ''
        'Content-Type':   if file.type != '' then file.type else 'application/octet-stream'
        filename:         data['fileName']
      file: file
    )
    upload.progress((evt) ->
        $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total)
    )
    upload.then (data, status, headers, config) ->
      console.log 'upload complete'
      parse()
    return upload