'use strict'

describe 'Controller: CaptureCtrl', ->

  # load the controller's module
  beforeEach module 'excipiensApp'
  CaptureCtrl = undefined
  scope = undefined

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    CaptureCtrl = $controller 'CaptureCtrl',
      $scope: scope

  it 'should ...', ->
    expect(1).toEqual 1
