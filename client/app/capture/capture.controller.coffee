'use strict'

angular.module 'excipiensApp'
.controller 'CaptureCtrl', ($scope, Upload, $timeout, $http, socket, Auth, $location, $uibModal, $log, alertService) ->

  $scope.showDelete = true
  $scope.getCurrentUser = Auth.getCurrentUser


  $http.get("/api/sources/available_tag_types")
    .success (data) ->
      console.log data
      $scope.availableTypes = data

  $http.get('/api/sources').success (sources) ->
    $scope.sources = sources
    socket.syncUpdates 'source', $scope.sources

  $scope.$watch 'files', ->
    $scope.upload $scope.files

  $scope.files = []

  $scope.upload = (files) ->
    if files and files.length
      for file in files
        do ->
          if file.type != "text/csv"
            alertService.add('warning', "the file #{file.name} was not uploaded: please note xcipi currently only accepts CSV uploads")
          else
            $scope.files.push file

  $scope.animationsEnabled = true

  $scope.selected = undefined
  
  $scope.loadTwitterSources = () ->
    modalInstance = $uibModal.open(
      animation: $scope.animationsEnabled
      templateUrl: 'app/capture/twitter_sources.html'
      controller: 'twitterSourcesCtrl'
      size: 'medium'
    )
    modalInstance.result.then ((twitterChoice) ->
      $scope.twitterChoice = twitterChoice
      $http.post(
        '/api/sources', 
        name:    "Twitter #{twitterChoice}"
        user_id: Auth.getCurrentUser()._id
        type:    "twitter/#{twitterChoice}"
        key:     twitterChoice
      ).then ((response) ->
          $scope.source = response.data
          $http(
            url: "api/sources/#{response.data._id}/parse/"
            method: 'GET'
          ).then ((response) ->
            $scope.fields = response.data
            $scope.parseComplete = true
            alertService.add('success', "the twitter source #{$scope.source.name} was successfully created and parsed")
          ), (response) ->
            #error
            console.log response
      ), (response) ->
        # called asynchronously if an error occurs
        # or server returns response with an error status.  

    ), ->
      $log.info 'Modal dismissed at: ' + new Date
      console.log $scope.twitterChoices


  $scope.loadSalesforceSources = () ->
    modalInstance = $uibModal.open(
      animation: $scope.animationsEnabled
      templateUrl: 'app/capture/salesforce_sources.html'
      controller: 'salesforceSourcesCtrl'
      size: 'medium'
    )
    modalInstance.result.then ((salesforceChoice) ->
      $scope.twitterChoice = twitterChoice
      $http.post(
        '/api/sources', 
        name:    "Twitter #{twitterChoice}"
        user_id: Auth.getCurrentUser()._id
        type:    "twitter/#{twitterChoice}"
        key:     twitterChoice
      ).then ((response) ->
          $scope.source = response.data
          $http(
            url: "api/sources/#{response.data._id}/parse/"
            method: 'GET'
          ).then ((response) ->
            $scope.fields = response.data
            $scope.parseComplete = true
            alertService.add('success', "the twitter source #{$scope.source.name} was successfully created and parsed")
          ), (response) ->
            #error
            console.log response
      ), (response) ->
        # called asynchronously if an error occurs
        # or server returns response with an error status.  

    ), ->
      $log.info 'Modal dismissed at: ' + new Date
      console.log $scope.twitterChoices

  $scope.$on '$destroy', ->
   socket.unsyncUpdates 'source'
   socket.unsyncUpdates 'tag'

.controller 'sourceCaptureCtrl', ($scope, $http, socket, alertService) ->

  $scope.isCollapsed = true

  $scope.addTag = ->
    tag = {}
    tag[$scope.newTagType] = $scope.newTagValue
    $http.put(
      '/api/sources/'+ $scope.source._id+'/add_tag'
      tags: [tag]
    ).then ((response) ->
      alertService.add('success', "the tags were successfully updated")
    ), (response) ->
      # called asynchronously if an error occurs
      # or server returns response with an error status.


  $scope.removeTag = (tag) ->
    $http.put(
      '/api/sources/'+ $scope.source._id+'/remove_tag'
      tag: tag
    ).then ((response) ->
      alertService.add('success', "the tag was successfully removed")
    ), (response) ->
      # called asynchronously if an error occurs
      # or server returns response with an error status.



  $scope.deleteSource = () ->
    $http.delete '/api/sources/' + $scope.source._id

  $scope.revert = () ->
    $http.get '/api/sources/' + $scope.source._id + '/revert/'

  $scope.save = (source) ->
    $http.put(
      '/api/sources/'+ $scope.source._id
      name: source.name
      represents: source.represents
    ).then ((response) ->
      alertService.add('success', "the source #{source.name} was successfully updated")
      console.log "updated"
      console.log response.data
    ), (response) ->
      # called asynchronously if an error occurs
      # or server returns response with an error status.

.controller 'twitterSourcesCtrl', ($scope, $uibModalInstance, Auth) ->

  $scope.getCurrentUser = Auth.getCurrentUser
  $scope.twitterChoices = []
  
  $scope.setTwitterChoice = (twitterChoice) ->
    $uibModalInstance.close(twitterChoice)

  $scope.ok = ->
    $uibModalInstance.close($scope.twitterChoices)
  
  $scope.cancel = ->
    $uibModalInstance.dismiss 'cancel'


.controller 'salesforceSourcesCtrl', ($scope, $uibModalInstance, Auth, $http) ->
  $scope.salesforceData = {}
  $scope.salesforceData.query = "SELECT Id, Name FROM Account"
  $scope.getCurrentUser = Auth.getCurrentUser

  $scope.submitSalesforceData = (form) ->
    console.log $scope.salesforceData
    $http.post(
      '/api/sources/get_salesforce_data'
      $scope.salesforceData
    ).then ((response) ->
      alertService.add('success', "the source #{source.name} was successfully updated")
      console.log "updated"
      console.log response.data
    ), (response) ->
      # called asynchronously if an error occurs
      # or server returns response with an error status.

  $scope.cancel = ->
    $uibModalInstance.dismiss 'cancel'

