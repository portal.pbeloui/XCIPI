'use strict'

angular.module 'excipiensApp'
.config ($stateProvider) ->
  $stateProvider.state 'capture',
    url: '/capture'
    templateUrl: 'app/capture/capture.html'
    controller: 'CaptureCtrl'