'use strict'

angular.module 'excipiensApp'
.controller 'SettingsCtrl', ($scope, User, Auth, $window, alertService, $http) ->

  $scope.currentUser = Auth.getCurrentUser()

  $scope.loginOauth = (provider) ->
    $window.location.href = '/auth/' + provider

  $scope.unlinkTwitter = ->
    $http.get(
      "/auth/twitter/unlink"
    ).then ((response) ->
      alertService.add('success', "the twitter account was successfully unlinked")
      $scope.currentUser = response.data
    ), (response) ->
      # called asynchronously if an error occurs
      # or server returns response with an error status.

  $scope.errors = {}

  $scope.changePassword = (form) ->
    $scope.submitted = true

    if form.$valid
      Auth.changePassword $scope.user.oldPassword, $scope.user.newPassword
      .then ->
        $scope.message = 'Password successfully changed.'

      .catch ->
        form.password.$setValidity 'mongoose', false
        $scope.errors.other = 'Incorrect password'
        $scope.message = ''
