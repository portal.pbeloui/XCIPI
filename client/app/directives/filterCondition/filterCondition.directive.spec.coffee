'use strict'

describe 'Directive: filterCondition', ->

  # load the directive's module and view
  beforeEach module 'excipiensApp'
  beforeEach module 'app/directives/filterCondition/filterCondition.html'
  element = undefined
  scope = undefined
  beforeEach inject ($rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<filter-condition></filter-condition>'
    element = $compile(element) scope
    scope.$apply()
    expect(element.text()).toBe 'this is the filterCondition directive'

