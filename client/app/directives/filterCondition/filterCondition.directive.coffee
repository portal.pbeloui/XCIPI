'use strict'

angular.module 'excipiensApp'
.directive 'filterCondition', ->
  templateUrl: 'app/directives/filterCondition/filterCondition.html'
  restrict: 'EA'
  scope: 
  	condition: '='
  link: (scope, element, attrs) ->
  
  controller: ($scope) ->
    console.log $scope.condition

    $scope.removeCondition = (condition) ->
      $scope.$parent.filterConditions.splice($scope.$parent.filterConditions.indexOf(condition), 1)

    $scope.removeFilterField = ->
       $scope.condition.field = null
     
    $scope.onDropComplete = (data, evt) ->
      $scope.condition.field = data
