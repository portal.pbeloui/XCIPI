'use strict'

angular.module 'excipiensApp'
.directive 'sourceFilter', ->
  templateUrl: 'app/directives/sourceFilter/sourceFilter.html'
  restrict: 'EA'
  scope:
  	source: '='
  link: (scope, element, attrs) ->

  controller: ($scope, $element, $http, alertService) ->

    $scope.filterConditions = [{field: null, operator: '==', value: ''}]

    $scope.isCollapsed = true

    $scope.deleteSource = () ->
      $http.delete '/api/sources/' + $scope.source._id

    $scope.addFilterCondition = () ->
      filterCondition = 
           operator: '=='
           field: null
           value: null
      $scope.filterConditions.push(filterCondition)
    
    $scope.filter = () ->
      $http.post(
        '/api/sources/filter/'
        { 
          conditions: $scope.filterConditions
          source_id:  $scope.source._id
        }
      ).then ((response) ->
        console.log "updated"
        console.log response.data
        alertService.add('success', "the filtering was successful and the source '#{response.data.name}' was created")
      ), (response) ->
      	alertService.add('warning', response.data)
        # called asynchronously if an error occurs
        # or server returns response with an error status. 