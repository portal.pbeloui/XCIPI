'use strict'

describe 'Directive: sourceFilter', ->

  # load the directive's module and view
  beforeEach module 'excipiensApp'
  beforeEach module 'app/directives/sourceFilter/sourceFilter.html'
  element = undefined
  scope = undefined
  beforeEach inject ($rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<source-filter></source-filter>'
    element = $compile(element) scope
    scope.$apply()
    expect(element.text()).toBe 'this is the sourceFilter directive'

