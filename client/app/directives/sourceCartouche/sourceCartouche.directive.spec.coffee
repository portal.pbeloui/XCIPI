'use strict'

describe 'Directive: sourceCartouche', ->

  # load the directive's module and view
  beforeEach module 'excipiensApp'
  beforeEach module 'app/directives/sourceCartouche/sourceCartouche.html'
  element = undefined
  scope = undefined
  beforeEach inject ($rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<source-cartouche></source-cartouche>'
    element = $compile(element) scope
    scope.$apply()
    expect(element.text()).toBe 'this is the sourceCartouche directive'

