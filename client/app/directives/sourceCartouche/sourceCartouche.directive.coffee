'use strict'

angular.module 'excipiensApp'
.directive 'sourceCartouche', ->
  templateUrl: 'app/directives/sourceCartouche/sourceCartouche.html'
  restrict: 'EA'
  link: (scope, element, attrs) ->

  controller: ($scope, $element, $rootScope, $http, $uibModal) ->
  	$scope.viewData = () ->
      modalInstance = $uibModal.open(
        templateUrl: 'app/viewData/viewData.html'
        controller: 'viewSourceCtrl'
        size: 'lg'
        resolve: source: ->
          return $scope.source
      )