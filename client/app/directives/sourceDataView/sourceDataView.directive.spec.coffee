'use strict'

describe 'Directive: dataView', ->

  # load the directive's module and view
  beforeEach module 'excipiensApp'
  beforeEach module 'app/directives/sourceDataView/sourceDataView.html'
  element = undefined
  scope = undefined
  beforeEach inject ($rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<data-view></data-view>'
    element = $compile(element) scope
    scope.$apply()
    expect(element.text()).toBe 'this is the dataView directive'

