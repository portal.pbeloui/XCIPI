'use strict'

angular.module 'excipiensApp'
.directive 'sourceDataView', ->
  templateUrl: 'app/directives/sourceDataView/sourceDataView.html'
  restrict: 'EA'
  scope:
    source: '='
  link: (scope, element, attrs) ->
  controller: ($scope, $element, $rootScope, $http) ->

    $scope.chartObject = {}
    $scope.chartObject.type = 'Table'
    $scope.chartObject.data = {}
    $http.get("/api/sources/#{$scope.source._id}/data_for_chart").success (data) ->
      $scope.chartObject.data = data