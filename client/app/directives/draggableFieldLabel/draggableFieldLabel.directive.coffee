'use strict'

angular.module 'excipiensApp'
.directive 'draggableFieldLabel', ->
  templateUrl: 'app/directives/draggableFieldLabel/draggableFieldLabel.html'
  restrict: 'E'
  replace: true
  scope:
  	field:  '='
  	source: '='
  controller: ($scope, $element, $rootScope) ->
  	$scope.field.source = {}
  	$scope.field.source._id  = $scope.source._id
  	$scope.field.source.name = $scope.source.name

  link: (scope, element, attrs) ->