'use strict'

describe 'Directive: draggableFieldLabel', ->

  # load the directive's module and view
  beforeEach module 'excipiensApp'
  beforeEach module 'app/draggableFieldLabel/draggableFieldLabel.html'
  element = undefined
  scope = undefined
  beforeEach inject ($rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<draggable-field-label></draggable-field-label>'
    element = $compile(element) scope
    scope.$apply()
    expect(element.text()).toBe 'this is the draggableFieldLabel directive'

