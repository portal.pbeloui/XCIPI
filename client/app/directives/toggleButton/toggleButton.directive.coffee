'use strict'

angular.module 'excipiensApp'
.directive 'toggleButton', ->
  templateUrl: 'app/directives/toggleButton/toggleButton.html'
  restrict: 'E'
  replace: true
  scope:
  	axes: '='
  	field: '='
  controller: ($scope, $element, $rootScope) ->
    $scope.values = [
      ''
      'x'
      'y'
    ]

    $scope.changeTo = (index) ->
      $scope.selectedValue = if index < $scope.values.length then index else 0

    $scope.next = ->
      $scope.selectedValue = ($scope.selectedValue + 1) % $scope.values.length
      if $scope.selectedValue == 1
      	$scope.axes.x = $scope.field.name
      	$rootScope.$broadcast('axis-changed', {axis: 'x', field: $scope.field});
      if $scope.selectedValue == 2
      	$scope.axes.x = ''
      	$scope.axes.y = $scope.field.name
      	$rootScope.$broadcast('axis-changed', {axis: 'y', field: $scope.field});
      if $scope.selectedValue == 0
      	$scope.axes.y = ''
      	$scope.axes.x = ''

    $scope.$on 'axis-changed', (event, arg) ->
      if $scope.field._id != arg.field._id
      	if arg.axis == 'y' and $scope.selectedValue == 2
      	  $scope.selectedValue = 0
      	if arg.axis == 'x' and $scope.selectedValue == 1
      	  $scope.selectedValue = 0
    $scope.selectedValue = 0

  link: (scope, element, attrs) ->
