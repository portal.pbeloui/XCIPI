'use strict'

angular.module 'excipiensApp'
.directive 'fieldRemoveButton', ->
  templateUrl: 'app/directives/fieldRemoveButton/fieldRemoveButton.html'
  restrict: 'E'
  replace: true
  scope:
    field: '='
    sourceId: '@'
  controller: ($scope, $element, $rootScope, $http, alertService) ->
    $scope.removing = false


    $scope.onDropComplete = (draggedField, evt) ->
      path = '/api/sources/'+ $scope.$parent.source._id + '/reorder_fields/' + $scope.field._id + '/' + draggedField._id
      $http.put(
        path
        $scope.field
      ).then ((response) ->

      ), (response) ->
        # called asynchronously if an error occurs
        # or server returns response with an error status. 


    $scope.removeField = ->
      name = $scope.field.name
      $scope.removing = true
      $http.delete(
        "api/sources/#{$scope.sourceId}/delete_field/#{$scope.field._id}"
        field:     $scope.field
        source_id: $scope.sourceId
      ).then ((response) ->
        alertService.add('success', "the field #{name} was removed.")
        $scope.removing = false
      ), (response) ->
        $scope.removing = false
        # called asynchronously if an error occurs
        # or server returns response with an error status. 

  link: (scope, element, attrs) ->
