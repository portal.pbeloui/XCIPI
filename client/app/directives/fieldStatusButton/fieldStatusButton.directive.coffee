'use strict'

angular.module 'excipiensApp'
.directive 'fieldStatusButton', ->
  templateUrl: 'app/directives/fieldStatusButton/fieldStatusButton.html'
  restrict: 'E'
  replace: true
  scope:
    field: '='
    isFor: '@'
  controller: ($scope, $element, $rootScope, $http) ->
    console.log($scope.isFor);
    $scope.next = ->
      $scope.field.is_identifier = !$scope.field.is_identifier
      path = '/api/sources/'+ $scope.$parent.source._id + '/update_field/' + $scope.field._id
      $http.put(
        path
        $scope.field
      ).then ((response) ->
        console.log "updated"
        console.log response.data
      ), (response) ->
        # called asynchronously if an error occurs
        # or server returns response with an error status. 

  link: (scope, element, attrs) ->
