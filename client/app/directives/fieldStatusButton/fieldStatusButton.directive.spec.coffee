'use strict'

describe 'Directive: fieldStatusButton', ->

  # load the directive's module and view
  beforeEach module 'excipiensApp'
  beforeEach module 'app/fieldStatusButton/fieldStatusButton.html'
  element = undefined
  scope = undefined
  beforeEach inject ($rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<field-status-button></field-status-button>'
    element = $compile(element) scope
    scope.$apply()
    expect(element.text()).toBe 'this is the fieldStatusButton directive'

