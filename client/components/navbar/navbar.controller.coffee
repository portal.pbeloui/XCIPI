'use strict'

angular.module 'excipiensApp'
.controller 'NavbarCtrl', ($scope, $location, Auth, alertService, $rootScope) ->

  $scope.alert = []

  $scope.closeAlert = (alert) ->
    alertService.closeAlert(alert)

  $rootScope.$on 'updateAlerts', (event, data) ->
    $scope.alerts = alertService.get()

  #{title: 'Sort', link:  '/sort'}
  $scope.menu = [
    {title: 'Capture' , link: '/capture'}
    {title: 'Blend',    link:  '/blend'}
    {title: 'Refine',    link:  '/refine'}
    {title: 'Filter',   link:  '/filter'}
    {title: 'Explore',  link:  '/explore'}
  ]
  $scope.isCollapsed = true
  $scope.isLoggedIn = Auth.isLoggedIn
  $scope.isAdmin = Auth.isAdmin
  $scope.getCurrentUser = Auth.getCurrentUser

  $scope.logout = ->
    Auth.logout()
    $location.path '/login'

  $scope.isActive = (route) ->
    route is $location.path()