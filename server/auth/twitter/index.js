'use strict';

var express = require('express');
var passport = require('passport');
var auth = require('../auth.service');

var router = express.Router();

router
    .get('/', passport.authenticate('twitter', {
        failureRedirect: '/settings',
        session: false
    }))

    .get('/callback', auth.addAuthHeaderFromCookie(), auth.appendUser(), passport.authenticate('twitter', {
        failureRedirect: '/settings',
        session: false
    }), auth.setTokenCookie)

    .get('/unlink', auth.isAuthenticated(), function(req, res) {
            var user           = req.user;
            user.twitter = undefined;
            user.save(function(err) {
               return res.status(200).json(user);
            });
        });

module.exports = router;