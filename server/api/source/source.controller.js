'use strict';

var _            = require('lodash');
var Source       = require('./source.model');
var AWS          = require('aws-sdk');
var Chance       = require('chance');
var Policy       = require('s3-policy');
var csv          = require('express-csv');
var neo4j        = require('neo4j');
var Twitter      = require('twitter-node-client').Twitter;
var config       = require('../../config/environment');
var CSVConverter = require("csvtojson").Converter;
var fs           = require('fs');
var paginate     = require('express-paginate');
var jsforce      = require('jsforce');
var watson       = require('watson-developer-cloud');
var async        = require("async");


var alchemy_language = watson.alchemy_language({
  api_key: 'af36ce7601cf85b1dd02ffc9a19af248d854f1f9'
});


AWS.config.loadFromPath('server/config/aws.json');

// Define the callback function for array.every <- super ugly, needs to be moved
function isTrue(value, index, ar) {
    return value;
}


// Get list of sources
exports.index = function(req, res) {
  Source.find({user_id: req.user._id}).sort('name').find(function (err, sources) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(sources);
  });
};


// Get a single source
exports.show = function(req, res) {
  Source.findById(req.params.id, function (err, source) {
    if(err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
    return res.json(source);
  });
};

// Get a single source
exports.data = function(req, res) {
  Source.findById(req.params.id, function (err, source) {
    if(err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
    //var data = restoreData(source);
    return res.json(source.data);
  });
};

// Get a single source
exports.data_for_chart = function(req, res) {
  Source.findById(req.params.id, function (err, source) {
    if(err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
    
    var orderedFields = _.sortBy(source.fields, 'position');


    var cols = [];
    for(var field of orderedFields){
      cols.push({
        id: field.inner_id.toString(),
        label: field.name,
        type: field.type.toLowerCase()
      });
    }

    var rows = [];

    for(var obj of source.data){
      var row = {'c': []};
      for(var col of cols){ 
        row['c'].push({'v': obj[col.id]});
      }
      rows.push(row);
    }
    var dataForChart = {'cols': cols, 'rows': rows}
    return res.json(dataForChart);
  });
};


// Get salesforce data
exports.getSalesforceData = function(req, res) {
  console.log(req.body);
  var username = req.body.username;
  var password = req.body.password;
  var securityToken = req.body.securityToken;
  var query    = req.body.query;
  
  var conn = new jsforce.Connection();
  conn.login(username, password+securityToken, function(err, res) {
    if (err) { return console.error(err); }
    conn.query(query, function(err, res) {
      if (err) { return console.error(err); }
      console.log(res);
    });
  });


};


// rollback source
exports.revert = function(req, res) {
  Source.findById(req.params.id, function (err, source) {
    if(err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
    if(source._version <= 0) { return res.status(403).send('You cannot revert further'); }
    source.revert(source._version-1, function(err, hist) {
      if (err) throw (err);
      return res.json(hist);
    });    
  });
};


// Creates a new source in the DB.
exports.create = function(req, res) {
  Source.create(req.body, function(err, source) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(source);
  });
};

// Updates an existing source in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Source.findById(req.params.id, function (err, source) {
    if (err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
    var updated = _.merge(source, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(source);
    });
  });
};

// Updates an existing source in the DB.
exports.add_tag = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Source.findById(req.params.id, function (err, source) {
    if (err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
    if(req.body.tags.length > 0){
      for(var tag of req.body.tags){
        source.tags.push(tag);
      }
    }
    source.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(source);
    });
  });
};


// Updates an existing source in the DB.
exports.remove_tag = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Source.findById(req.params.id, function (err, source) {
    if (err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }    
    source.tags.splice( source.tags.indexOf(req.body.tag), 1 );
    source.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(source);
    });
  });
};




// Updates an existing source in the DB.
exports.export_as_file = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Source.findById(req.params.id, function (err, source) {
    if (err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
    res.csv([
       ["a", "b", "c"]
     ,["d", "e", "f"]
    ]);
    return res.status(200);
  });
};

// Updates an existing field for a source in the DB.
exports.update_field = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Source.findById(req.params.id, function (err, source) {
    if (err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
    var field = source.fields.id(req.params.field_id);

    field.is_identifier = req.body.is_identifier;
    source.save(function (err,saved_source) {
      if (err) return handleError(err);
      return res.status(200).json(source);
    });
  });
};

// Analyses field sentiment with watson api
exports.analyse_field_sentiment = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Source.findById(req.params.id, function (err, source) {
    if (err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
    var sentimentField = source.fields.id(req.params.sentiment_field_id);
    var xAxisField = source.fields.id(req.params.x_axis_field_id);
    var obj;
    var data = source.data;
    var counter = 0;
    var results = [];
 

    async.each(data,
      // 2nd param is the function that each item is passed to
      function(obj, callback){
        // Call an asynchronous function, often a save() to DB
        var params = {
          text: obj[sentimentField.inner_id]
        };
        alchemy_language.sentiment(params, function (err, response) {
          if (err){
            console.log('error:', err);
          }
          else {
            var toInsert = response;
            _.assign(toInsert, toInsert['docSentiment']);
            delete toInsert['docSentiment'];
            _.assign(toInsert, obj);
            delete toInsert['usage'];
            delete toInsert['status'];
            delete toInsert['totalTransactions'];
            delete toInsert['language'];
            //toInsert['score'] = parseFloat(toInsert['score'], 10).toFixed(2);
            results.push(_.assign(response, obj));
            callback();
          }
        });
        },
        // 3rd param is the function to call when everything's done
        function(err){
          if (err) 
            return res.status(404).send('Not Found');
          else
            // All tasks are done now
            var sortField = xAxisField.inner_id.toString();
            var sorted = _.sortBy(results, sortField);
            console.log("SORTED");
            console.log(sorted);
            return res.status(200).json(sorted);
        }
    );

  });
};

// Updates an existing field for a source in the DB.
exports.reorder_fields = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Source.findById(req.params.id, function (err, source) {
    if (err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
    
    var firstField  = source.fields.id(req.params.first_field_id);
    var secondField = source.fields.id(req.params.second_field_id);
    
    var bufferPosition = firstField.position
    firstField.position = secondField.position
    secondField.position = bufferPosition
    
    source.save(function (err,saved_source) {
      if (err) return handleError(err);
      return res.status(200).json(source);
    });

  });
};

// Deletes an existing field for a source in the DB.
exports.delete_field = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Source.findById(req.params.id, function (err, source) {
    if (err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
      if(source.fields.id(req.params.field_id)){
        var obj;
        var field = source.fields.id(req.params.field_id);
        field.remove();
        var data = source.data;
        for(obj of data){
          delete obj[field.name];
        }
        // setting to null is required for the save to work properly, I'm not sure why :/
        source.data = null;
        source.data = data;
        source.save(function (err,saved_source) {
          if (err) return handleError(err);
          return res.status(200).json(saved_source);
        });
      }
  });
};

// Deletes a source from the DB.
exports.destroy = function(req, res) {
  Source.findById(req.params.id, function (err, source) {
    if(err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
    source.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

exports.sign = function(req,res) {
  var chance   = new Chance();
  var bucket   = 'excipiens-dev'
  var s3 = new AWS.S3();
  var key = chance.guid();
  var p = Policy({
    //will have to put the secret in a more secure place - works for the time being
    secret: 'tZvRkkMyLUzEm2IDp066IMN26zt/TsLTQlPe6E1Y',
    length: 5000000,
    bucket: bucket,
    key: key,
    expires: new Date(Date.now() + 60000),
    acl: 'public-read'
  });
  return res.json({policy: p.policy, signature: p.signature, key: key, AWSAccessKeyId: AWS.config.credentials.accessKeyId});
}

exports.parse = function(req, res) {
  Source.findById(req.params.id, function (err, source) {
    if(err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
    switch(source.type) {
        case "text/csv":
          var s3 = new AWS.S3();
          var params = {Bucket: 'excipiens-dev', Key: source.key}
          var stream = s3.getObject(params).createReadStream();
          var converter = new CSVConverter({constructResult:true});
          //end_parsed will be emitted once parsing finished 
          converter.on("end_parsed", function (parsedData) {
            if(!parsedData.length == 0){
              source.fields = createFields(parsedData);
              source.data   = setupData(parsedData, source.fields);
              source.save();
              return res.status(200).json([]);
            }else{
              source.remove();
              return res.status(404).send('The file you uploaded was empty. Thus, the source was not created.')
            }
          });
          //read from file 
          stream.pipe(converter);
        case "test":
          var test = 'test';
          break;
        case "twitter/followers":
          var twitterConfig = {
            "consumerKey": config.twitter.clientID,
            "consumerSecret": config.twitter.clientSecret,
            "accessToken": req.user.twitter.token,
            "accessTokenSecret": config.twitter.accessTokenSecret,
            "callBackUrl": config.twitter.callbackURL
          };        
          //Callback functions
          var error = function (err, response, body) {
            console.log('ERROR [%s]', err);
            console.log(err);
          };
          var success = function (dataString) {
            var parsedData = JSON.parse(dataString).users;
            if(!parsedData.length == 0){
              source.fields = createFields(parsedData);
              source.data   = setupData(parsedData, source.fields);
              source.save();
              return res.status(200).json(source);
            }else{
              source.remove();
              return res.status(404).send('The twitter feed was empty. Thus, the source was not created.')
            }
          };
          
          var twitter = new Twitter(twitterConfig);
          console.log(req.user.twitter);
          twitter.getFollowersList({'user_id': req.user.twitter.id_str}, error, success);
          break;
        case "twitter/mentions":
          var twitterConfig = {
            "consumerKey": config.twitter.clientID,
            "consumerSecret": config.twitter.clientSecret,
            "accessToken": req.user.twitter.token,
            "accessTokenSecret": config.twitter.accessTokenSecret,
            "callBackUrl": config.twitter.callbackURL
          };          

          //Callback functions
          var error = function (err, response, body) {
            //console.log('ERROR [%s]', err);
            console.log(err);
          };
          var success = function (dataString) {
            var parsedData = JSON.parse(dataString);
            if(!parsedData.length == 0){
              source.fields = createFields(parsedData);
              source.data   = setupData(parsedData, source.fields);
              source.save();
              return res.status(200).json([]);
            }else{
              source.remove();
              return res.status(404).send('The twitter feed was empty. Thus, the source was not created.')
            }

          };

          var twitter = new Twitter(twitterConfig);
          twitter.getMentionsTimeline({'screen_name': req.user.twitter.name}, error, success);
          break;
        case "twitter/retweets":
          var twitterConfig = {
            "consumerKey": config.twitter.clientID,
            "consumerSecret": config.twitter.clientSecret,
            "accessToken": req.user.twitter.token,
            "accessTokenSecret": config.twitter.accessTokenSecret,
            "callBackUrl": config.twitter.callbackURL
          };
          //Callback functions
          var error = function (err, response, body) {
            console.log('ERROR [%s]', err);
            console.log(response);
          };
          var success = function (dataString) {
            var parsedData = JSON.parse(dataString);
            if(!parsedData.length == 0){
              source.fields = createFields(parsedData);
              source.data   = setupData(parsedData, source.fields);
              source.save();
              return res.status(200).json([]);
            }else{
              source.remove();
              return res.status(404).send('The twitter feed was empty. Thus, the source was not created.')
            }
          };

          var twitter = new Twitter(twitterConfig);
          twitter.getReTweetsOfMe({'screen_name': req.user.twitter.name}, error, success);
          break;
        case "twitter/timeline":
          var twitterConfig = {
            "consumerKey": config.twitter.clientID,
            "consumerSecret": config.twitter.clientSecret,
            "accessToken": req.user.twitter.token,
            "accessTokenSecret": config.twitter.accessTokenSecret,
            "callBackUrl": config.twitter.callbackURL
          };
          //Callback functions
          var error = function (err, response, body) {
            console.log('ERROR [%s]', err);
            console.log(response);
          };
          var success = function (dataString) {
            var parsedData = JSON.parse(dataString);
            if(!parsedData.length == 0){
              source.fields = createFields(parsedData);
              source.data   = setupData(parsedData, source.fields);
              source.save();
              return res.status(200).json([]);
            }else{
              return res.status(404).send('The twitter feed was empty. Thus, the source was not created.')
            }
          };

          var twitter = new Twitter(twitterConfig);
          twitter.getUserTimeline({'screen_name': req.user.twitter.name}, error, success);
          break;
        default:
          return res.json([]);
    }
  });
};

// parse new blend: create a source as its product
exports.blend = function(req, res) {
  var sourceIds   = [];
  var fieldNames  = [];
  var sourceNames = [];
  
  var joinField;
  for (joinField of req.body) {
    fieldNames.push({name: joinField.name, sourceId: joinField.source._id});
    sourceIds.push(joinField.source._id);
    sourceNames.push(joinField.source.name);
  }
  
  Source.find({
    '_id': { $in: sourceIds}
    }, function(err, sources){
    var allMatchedObjects = []   
    var source;
    for (source of sources){
      var fieldName;
      for (fieldName of fieldNames){
        var valuesForSource = [];
        if(source._id == fieldName.sourceId){
          var valuesForSource = restoreData(source).map(function(a) {return a[fieldName.name];});
        }
        var joinSource;
        for (joinSource of sources){
          if(joinSource._id != source._id){
            var joinFieldName;
            for (joinFieldName of fieldNames){
              if(joinSource._id == joinFieldName.sourceId){
                var matchingObjects = restoreData(joinSource).filter(function( obj ) {
                  obj['joinField'] = obj[joinFieldName.name];
                  return valuesForSource.indexOf(obj[joinFieldName.name]) > -1;
                });
                allMatchedObjects = allMatchedObjects.concat(matchingObjects);
              }
            }
        }
        }
      }
    }
    //final clean
    var obj;
    var blendData = []
    for (obj of allMatchedObjects) {
      var sObj;
      for (sObj of allMatchedObjects) {
        if(obj.joinField == sObj.joinField){
          for (var attrname in sObj) { obj[attrname] = sObj[attrname]; }
            
          var found = false;
          for(var i = 0; i < blendData.length; i++) {
              if (blendData[i].joinField == obj.joinField) {
                found = true;
                break;
              }
          }
          if(!found){
            blendData.push(obj);
          }
                    
        }
      }
    }

    var cleanBlendData = [];
    //remove unwanted fields
    for(obj of blendData){
      cleanBlendData.push(_.omit(obj,'joinField'));
    }
    if(cleanBlendData.length > 0){
      var fields = createFields(cleanBlendData);
      var sourceData = {
        fields: fields,
        data: setupData(cleanBlendData,fields),
        type: 'blend',
        name: 'blend result of '+ sourceNames.join(" and "),
        user_id: req.user._id,
        join_field: req.body
      }
      Source.create(sourceData
        , function(err, newSource) {
        if(err) { return handleError(res, err); }
        return res.status(201).json(newSource);
      });
    }
  });
};


// filter source: creates a new source as its product
exports.filter = function(req, res) {

  var conditions = req.body.conditions;
  var source_id  = req.body.source_id;
  var filteredData = [];
  //find source
  Source.findById(source_id, function (err, source) {
    if(err) { return handleError(res, err); }
    if(!source) { return res.status(404).send('Not Found'); }
    var obj;
    for (obj of source.data){
      var condition;
      var field;
      var passes_all_conditions = [];
      for (field in obj){
        for (condition of conditions) {

          if((condition.field == null) || (condition.value == null)){return res.status(404).send('One of your conditions is empty - Please make sure each of your condition has a field and a value')}
          if(field == condition.field.inner_id){
            var passes_this_condition = false;
            if(condition.field.type == "Number" && (!isNaN(parseFloat(obj[condition.field.inner_id])))){
              if(eval(obj[condition.field.inner_id]+condition.operator+condition.value)){
                passes_this_condition = true;
              }            
              else {
                 passes_this_condition = false;
              }
            }
            //if not number, can only be string atm
            else{
             if(eval("'"+obj[condition.field.inner_id]+"'"+condition.operator+"'"+condition.value+"'")){
                passes_this_condition = true;
             }
             else {
                passes_this_condition = false;
             }  
            }
          passes_all_conditions.push(passes_this_condition);
          }
        }
      }
      if(passes_all_conditions.every(isTrue) && (passes_all_conditions.length > 0)){
        filteredData.push(obj);
      }
    }

    if(filteredData.length > 0){

      var conditions_for_name = [];
      var condition;
      for (condition of conditions) {
        conditions_for_name.push(condition.field.name+" "+condition.operator+" "+condition.value);
      }
      var conditions_as_string = " where "+conditions_for_name.join(' and ');

      var sourceData = {
        fields: source.fields,
        data: filteredData,
        type: 'filter',
        name: 'filter result of '+ source.name + conditions_as_string,
        user_id: req.user._id
      }
      Source.create(sourceData
        , function(err, newSource) {
        if(err) { return handleError(res, err); }
        return res.status(201).json(newSource);
      });
    }
    else{
      return res.status(404).send("the filtering didn't return any results: thus, no new source was created.");
    }
  });
};


function createFields(data){
  var columns   = Object.keys(data[0]);
  var fields = []
  var index;
  for (index = 0; index < columns.length; ++index) {
    var fieldType = "String";
    var position  = index;
    var inner_id  = index;
    var name = columns[index];
    var firstRowValue = (data[0][name]);
    if (typeof firstRowValue != "number") {
      //it's a string by default, more types to come?
    }
    else {
      fieldType = "Number"
    }
    var field = {name: name, type: fieldType, position: position, inner_id: inner_id};
    fields.push(field);
  }
  return fields;
}


exports.available_tag_types = function(req,res) {

  Source.find({user_id: req.user._id}).find(function (err, sources) {
    if(err) { return handleError(res, err); }
    var allKeys = [];
    for (var source of sources){
      for(var tag of source.tags){
        allKeys.push(Object.keys(tag));
      }
    }
    allKeys = _.flatten(allKeys);
    allKeys = _.uniq(allKeys);
    return res.status(200).json(allKeys);
  }).select('tags -_id');
}


function setupData(data, fields){
  var newData = [];
  for(var row of data) {
    var newRow = {};
    for (var field of fields){
      newRow[field.inner_id] = row[field.name];
    }
    newData.push(newRow);
  }
  return newData;
}

function restoreData(source){
  var newData = [];
  for(var row of source.data) {
    var newRow = {};
    for (var field of source.fields){
      newRow[field.name] = row[field.inner_id];
    }
    newData.push(newRow);
  }
  return newData;  
}

function handleError(res, err) {
  return res.status(500).send(err);
}