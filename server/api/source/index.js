'use strict';

var express = require('express');
var controller = require('./source.controller');

var router = express.Router();
var auth = require('../../auth/auth.service');

router.get('/',auth.isAuthenticated(), controller.index);
router.get('/sign',auth.isAuthenticated(), controller.sign);
router.get('/:id/data',auth.isAuthenticated(), controller.data);
router.get('/:id/data_for_chart',auth.isAuthenticated(), controller.data_for_chart);
router.get('/:id/parse',auth.isAuthenticated(), controller.parse);
router.get('/:id/revert',auth.isAuthenticated(), controller.revert);
router.get('/:id/export_as_file',auth.isAuthenticated(), controller.export_as_file);

router.get('/available_tag_types',auth.isAuthenticated(), controller.available_tag_types);

router.post('/',auth.isAuthenticated(), controller.create);
router.post('/get_salesforce_data',auth.isAuthenticated(), controller.getSalesforceData);
router.post('/blend/',auth.isAuthenticated(), controller.blend);
router.post('/filter/',auth.isAuthenticated(), controller.filter);
router.put('/:id',auth.isAuthenticated(), controller.update);
router.put('/:id/add_tag',auth.isAuthenticated(), controller.add_tag);
router.put('/:id/remove_tag',auth.isAuthenticated(), controller.remove_tag);
router.delete('/:id',auth.isAuthenticated(), controller.destroy);

router.put('/:id/update_field/:field_id',auth.isAuthenticated(), controller.update_field);

router.get('/:id/analyse_field_sentiment/:sentiment_field_id/:x_axis_field_id',auth.isAuthenticated(), controller.analyse_field_sentiment);

router.put('/:id/reorder_fields/:first_field_id/:second_field_id',auth.isAuthenticated(), controller.reorder_fields);
router.delete('/:id/delete_field/:field_id',auth.isAuthenticated(), controller.delete_field);



module.exports = router;