'use strict';

var timestamps = require('mongoose-timestamp');

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var rollback = require('mongoose-rollback');


mongoose.plugin(timestamps,  {
  createdAt: 'created_at', 
  updatedAt: 'updated_at'
});

var SourceSchema = new Schema({
  name: String,
  represents: String,
  type: {
        type: String,
        enum : ['blend','filter','text/csv','twitter/followers','twitter/timeline','twitter/mentions','twitter/retweets'],
        default : 'text/csv'
    },
  user_id: String,
  fields: [{ name: String,
             inner_id: Number,
             position: Number, 
             is_identifier: {type: Boolean, default: false},
             type: {
               type: String, 
               enum: ['String','Number'], 
               default: 'String'
             }
          }],
  join_fields: [{ name: String, 
             is_identifier: {type: Boolean, default: false},
             inner_id: Number,
             position: Number,
             type: {
               type: String, 
               enum: ['String','Number'], 
               default: 'String'
             },
             source: {_id: String, name: String}
          }],
  key: String,
  data: {},
  tags: [{}]
});

SourceSchema.set('versionKey', false);

SourceSchema.set('toObject', { virtuals: true });
SourceSchema.set('toJSON', { virtuals: true });


SourceSchema
.virtual('data_size')
.get(function () {
  if (this.data == undefined){
    return 0;
  }
  else {
    return this.data.length;
  }
});

SourceSchema.options.toJSON.transform = function (doc, ret, options) {
  // remove the _id of every document before returning the result
  delete ret.data;
}

SourceSchema.plugin(timestamps);

SourceSchema.plugin(rollback, {
    index: true,
    collectionName: 'sources' 
});

module.exports = mongoose.model('Source', SourceSchema, 'sources');