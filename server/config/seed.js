/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var User   = require('../api/user/user.model');
var Source = require('../api/source/source.model');

User.find({}).remove(function() {
  User.create({
    provider: 'local',
    name: 'Test User',
    email: 'test@test.com',
    password: 'test'
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@admin.com',
    password: 'admin'
  }, function(err, user) {
        if(user.name ="Test User"){
          
          Source.find({}).remove(function() {
            Source.create({
              user_id: user._id,
              type: 'text/csv',
              name: 'first source',
              fields: [{name: 'shoes', type: "String", is_identifier: true, position: 0, inner_id: 0},{name: 'name', type: "String", is_identifier: true, position: 1, inner_id: 1}],
              data: [{'0': "adidas", '1': "pascal"},{'0': "nike", '1': "dik sum"},{'0': "reebok", '1': "willi"}],
              tags: [{'type': 'sales report'}, {'importance': "medium"}]
            },
            {
              user_id: user._id,
              type: 'text/csv',
              name: 'sentiment',
              fields: [{name: 'id', type: "Number", is_identifier: true, position: 0, inner_id: 0},{name: 'text', type: "String", is_identifier: false, position: 1, inner_id: 1}],
              data: [{'0': '0', '1': "Home Delivery – the service is always brilliant, the delivery guys are great, but I am getting really sick of the people who pick the products initially. So often I’ve tried to take advantage of an offer and they’ve swapped something so the offer doesn’t kick in (eg only providing two things in a “3 for £10″ deal so the two stay full price or providing full price equivalent (more expensive) of something that was on offer. They need more common sense for their item pickers, am getting sick of it."},{'0': '1', '1': "Visited the store at New Cross, London to buy some cream-only two were still within the sell by date.Told customer service and the reply was “bring them to me then”. Realised I needed some bread as the shelf was being filled up -again all this new bread was out of date by a fortnight.The stacker said its OK its just come out of the freezer."},{'0': '2', '1': "Took mouldy green coconut biscuits back/and second pack of mouldy biscuits. I was amazed at the indifference of customer services who merely offered a refund.Their excuse? not sainsbury lable they buy it in it is down to the supplier – nasty excuse. At Waitrose they note a product and check with the supplier and you get a written account of what went wrong-without even having to ask!"} ,{'0': '3', '1': "Sainsburys Coldhams Lane Cambridge – made a special trip out of my way for one item from the Deli counter as had had it before and was rather nice. Got to deli counter – all items covered as tho they were closed but store still open. I asked the counter assistant for the item and was told (very curtly) that they were short staffed and had to clean counter so were shutting early. I politely pointed out store still open and demanded he serve me which he did – visibly annoyed!"},{'0': '4', '1': "Used them twice for getting my shopping delivered, overall pretty good, but both times the soup I ordered was out of stock, which is a right pain, as its pretty essential to the recipe I am making. Other substitutions are annoying as they have always been far more expensive than the original item ordered. Also when using the Live Well For Less, if you try and order the shopping list for a meal plan, sometimes Sainsburys don’t sell all the stuff needed, which is a bit daft."},{'0': '5', '1': "I generally like Sainsbury’s but I have one major gripe with them right now, which is: I have been robbed by their self-checkout machines time and again. They seem to be running off a different database to the main tills because they never seem to recognise deals. Last night was yet another of MANY times this has happened. The deal was two bottles of Coke for £2.50. The self-checkout machine put them through at £1.50 each. No deal. Deals are skipped EVERY time I use one of these machines. "}]
            },            
            {
              user_id: user._id,
              type: 'text/csv',
              name: 'geo',
              fields: [{name: 'country', type: "String", is_identifier: true, position: 0, inner_id: 0},{name: 'number of people', type: "Number", is_identifier: false, position: 1, inner_id: 1}],
              data: [{'0': 'France', '1': 40},{'0': 'Germany', '1': 20},{'0': 'UK', '1': 12},{'0': 'Japan', '1': 340}]
            },
            {
              user_id: user._id,
              type: 'text/csv',
              name: 'second source',
              fields: [{name: 'person name', type: "String", is_identifier: true, position: 0, inner_id: 0},{name: 'number of friends', type: "Number", is_identifier: false, position: 1, inner_id: 1}],
              data: [{'0': "pascal", "1": 5},{'0': "dik sum", "1": 3},{'0': "willi", "1": 1}]
            }, function(err, sources) {
                console.log("sources & users created");
              }
            );
          });
          var sources = Source.find({});
        }
      }
  );
});