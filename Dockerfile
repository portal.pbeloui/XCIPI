FROM node

WORKDIR /home/xcipi

# Install Mean.JS Prerequisites

RUN apt-get install g++
RUN npm install -g node-gyp
RUN npm install -g grunt-cli
RUN npm install -g bower

# Install Mean.JS packages
ADD package.json /home/xcipi/package.json
RUN npm install

# Manually trigger bower. Why doesnt this work via npm install?
ADD .bowerrc /home/xcipi/.bowerrc
ADD bower.json /home/xcipi/bower.json
RUN bower install --config.interactive=false --allow-root

# Make everything available for start
ADD . /home/xcipi

# currently only works for development
ENV NODE_ENV development

# Port 3000 for server
# Port 35729 for livereload
EXPOSE 3000 35729
CMD ["grunt"]